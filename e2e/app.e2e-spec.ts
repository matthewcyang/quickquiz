import { QuickQuizPage } from './app.po';

describe('quick-quiz App', () => {
  let page: QuickQuizPage;

  beforeEach(() => {
    page = new QuickQuizPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
