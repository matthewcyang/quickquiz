import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizcardComponent } from './quizcard.component';

describe('QuizcardComponent', () => {
  let component: QuizcardComponent;
  let fixture: ComponentFixture<QuizcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
