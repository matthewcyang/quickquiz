import { ModuleWithProviders, NgModule } from '@angular/core';

import { QuizcardComponent }  from './quizcard.component';

// import {} from '../shared';

@NgModule({
    imports:[],
    declarations:[
        QuizcardComponent
    ],
    providers:[],
    exports:[
        QuizcardComponent
    ]
})
export class QuizcardModule {}