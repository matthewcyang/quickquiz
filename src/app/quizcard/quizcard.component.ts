import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quizcard',
  templateUrl: './quizcard.component.html',
  styleUrls: ['./quizcard.component.scss']
})
export class QuizcardComponent implements OnInit {

    @Input('isFlipped') flipCard: boolean;
  constructor() { }

  ngOnInit() {
  }

}
