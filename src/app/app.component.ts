import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { style, state, animate, transition, trigger } from '@angular/animations';
import { QuizdataService } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  flashCardFlipped: boolean = false;
  hasAnswered: boolean = false;
  score: number = 0;
  questions: any;
  currquestion: number=0;
  showResults:boolean = false;
  quizDone:boolean = false;
  quesCorrect: string = "Not Quite";
  question;
  prevQuestion;
  answer;

  fps = 60;
  duration = 2; // seconds
  start = 0; // pixel
  finish = 0;
  questionPadding = 35;
  distance;
  increment;
  position;
  time = 0;
  direction = -1 //toggle this to change direction
  handler:any;
  currstart = 0;

  questionFrame;
  questionItem;

  @ViewChild('questionbg') private questionBg: ElementRef;
  @ViewChild('questionholder2') private questionHolder: ElementRef;

  constructor(private quizData: QuizdataService, private myElement: ElementRef){}

  ngOnInit(){
    this.quizData.load().then((data)=>{
      // data.map((question) => {
      //   //do any transforms here
      //   return question;
      // });
      this.questions = data;
      // console.log("ques",this.questions);
      
    });
  }

  selectAnswer(answer, question){
      this.answer = answer;
      this.question = question;
      this.hasAnswered = true;
      answer.selected = true;
      question.flashCardFlipped = true;
      if(answer.correct){
        this.quesCorrect = "Correct!";
        this.score++;
      }
  }
  nextQuestion(){        
        //if currquestion > num questions, show results component
        if(this.currquestion==this.questions.length){
          this.showResults = true;
        }
        
        this.finish = this.myElement.nativeElement.getElementsByClassName("question--item_active")[0].offsetHeight + this.questionPadding;
        this.distance = this.finish - this.start;
        this.increment = this.distance / (this.duration * this.fps);
        this.handler = setInterval(this.move.bind(this), 1000 / this.fps);
        
        this.hasAnswered = false;
        this.answer.selected = false;
        
        this.currquestion++;
        this.prevQuestion = this.question;
        // this.question.flashCardFlipped = false;
        //****this is hacky attempt to mitigate some issues with screens where height is not enough (mobile landscape)
        this.questionFrame = this.myElement.nativeElement.getElementsByClassName("question--frame")[0];
        this.questionItem = this.myElement.nativeElement.getElementsByClassName("question--item_active")[0];
        console.log("activeitem height: "+this.questionItem.scrollHeight);
        if(this.questionFrame.offsetHeight < this.questionItem.scrollHeight){
          // this.questionFrame.style.height = this.questionItem.offsetWidth + "px";
          this.questionFrame.style.height = this.questionItem.scrollHeight+45+"px";
        }      
        //****/
        
  }
  restartQuiz(){
    this.score = 0;
    this.currquestion = 0;
    this.quizDone = false;
    this.hasAnswered = false;
    if(!!this.answer){
      this.answer.selected=false;
      this.question.flashCardFlipped = false;
      this.questionHolder.nativeElement.style.top = 0;
    }
    this.quesCorrect = "Not Quite";
    this.questionBg.nativeElement.style.bottom = 0;
    this.currstart = 0;
      
  }
  

  move() {
     
    // console.log("move called");
      this.time += 1 / this.fps;
      this.position = this.easeInOutQuint(this.time * 100 / this.duration, this.time, this.start, this.finish, this.duration);
        // console.log(this.position +" "+this.finish);

      if (this.position >= this.finish) {
        //console.log("done moving");
        //console.log(this.handler);

          clearInterval(this.handler);
          this.questionHolder.nativeElement.style.top = this.direction*(this.finish+this.currstart) + 'px';
          this.questionBg.nativeElement.style.bottom = this.direction*(this.finish+this.currstart) + 'px';
        
          this.currstart += this.finish;
          this.time = 0;
          this.quesCorrect = "Not Quite";
          this.prevQuestion.flashCardFlipped = false;
         
          //console.log("done");
          //console.log(this.start + " "+this.finish+" "+this.currstart);
          if(this.currquestion === this.questions.length){
            this.quizDone = true;
          }
          return;
      }
      // console.log("moving "+this.position);
      this.questionHolder.nativeElement.style.top = this.direction*(this.position+this.currstart) + 'px';
      this.questionBg.nativeElement.style.bottom = this.direction*(this.position+this.currstart) + 'px';

  }
  // formula     http://easings.net/
  // http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js
  // x: percent
  // t: current time,
  // b: beginning value,
  // c: change in value,
  // d: duration
  easeInOutQuad(x, t, b, c, d) {
      if ((t /= d / 2) < 1) {
          return c / 2 * t * t + b;
      } else {
          return -c / 2 * ((--t) * (t - 2) - 1) + b;
      }
  }
  easeInOutQuint (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
    return c/2*((t-=2)*t*t*t*t + 2) + b;
  }

}
