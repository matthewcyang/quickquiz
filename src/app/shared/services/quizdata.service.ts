import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class QuizdataService {
    data: any;

    constructor(private _http: Http){

    }
    load(){
        if(this.data){
            return Promise.resolve(this.data);
        }
        return new Promise(resolve => {
            this._http.get('assets/data/questions.json').map(res => res.json())
                .subscribe(data =>{
                    this.data = data.questions;
                    resolve(this.data);
                });
        });
    }

}