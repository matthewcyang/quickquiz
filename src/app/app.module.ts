import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { QuizcardModule } from './quizcard/quizcard.module';

import {
  QuizdataService
} from './shared';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    QuizcardModule
  ],
  providers: [
    QuizdataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
